import cats.effect.ExitCode
import com.typesafe.scalalogging.LazyLogging
import config.Config
import db.Database
import monix.eval.{Task, TaskApp}

object WebServer extends TaskApp with LazyLogging {
  override def run(args: List[String]): Task[ExitCode] =
    Config().flatMap(initService).onErrorHandle{e =>
      logger.error("Failed to read config", e)
      ExitCode.Error
    }

  private def initService(config: Config) = resource(config).use(_ => Task.never[ExitCode])

  private def resource(config: Config) =
    for {
      xa <- new Database(config.dbConf).transactor
    } yield xa
}
