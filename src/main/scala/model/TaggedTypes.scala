package model

import doobie._
import supertagged.TaggedType

object TaggedTypes {

  trait MetaSupport[T] extends TaggedType[T] {
    implicit def liftedMeta(implicit meta: Meta[T]): Meta[Type] = apply(meta)
  }

  object Id extends TaggedType[Long] with MetaSupport[Long]
  type Id = Id.Type

  object Firstname extends TaggedType[String] with MetaSupport[String]
  type Firstname = Firstname.Type

  object Lastname extends TaggedType[String] with MetaSupport[String]
  type Lastname = Lastname.Type

  object Age extends TaggedType[Int] with MetaSupport[Int]
  type Age = Age.Type

  object Name extends TaggedType[String] with MetaSupport[String]
  type Name = Name.Type

  object Value extends TaggedType[String] with MetaSupport[String]
  type Value = Value.Type
}