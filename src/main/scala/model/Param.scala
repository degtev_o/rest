package model

import model.TaggedTypes.{Name, Value}

case class Param(name: Name, value: Value)