package model

import model.TaggedTypes.{Id, Name}

case class Group(id: Id, name: Name, params: Seq[Param])