package model

import model.GenderEnum.Gender
import model.TaggedTypes.{Age, Firstname, Id, Lastname}

case class User(id: Id, firstname: Firstname, lastname: Lastname, age: Age, gender: Gender)

object GenderEnum extends Enumeration {
  type Gender = Value

  val Male = Value("M")
  val Female = Value("F")
  val Bigender = Value("B")
  val Agender = Value("A")
}