package service

import doobie.hikari.HikariTransactor
import monix.eval.Task

case class MasterService(user: UserService, group: GroupService)

object MasterService {

  def fromTransactor(xa: HikariTransactor[Task]): MasterService = MasterService(
    UserService.fromTransactor(xa),
    GroupService.fromTransactor(xa)
  )
}