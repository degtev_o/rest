package service

import db.repo.UserRepo
import doobie.implicits._
import doobie.hikari.HikariTransactor
import model.User
import monix.eval.Task

trait UserService {

  def getById(id: Long): Task[User]
  def getAll: Task[Seq[User]]
}

object UserService {

  def fromTransactor(xa: HikariTransactor[Task]): UserService = new UserService {

    def getById(id: Long): Task[User] = UserRepo.getById(id).transact(xa)
    def getAll: Task[Seq[User]] = UserRepo.getAll.transact(xa)
  }
}