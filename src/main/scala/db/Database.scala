package db

import cats.effect.Resource
import config.DbConf
import doobie.hikari.HikariTransactor
import doobie.util.ExecutionContexts
import monix.eval.Task

class Database(dbConf: DbConf) {

  val transactor: Resource[Task, HikariTransactor[Task]] =
    for {
      ce <- ExecutionContexts.fixedThreadPool[Task](dbConf.connectionPoolSize)
      te <- ExecutionContexts.cachedThreadPool[Task]
      xa <- HikariTransactor.newHikariTransactor[Task](
        dbConf.driver, dbConf.connection, dbConf.user,
        dbConf.password, ce, te)
    } yield xa
}
