package db.repo

import doobie._
import doobie.implicits._
import model.GenderEnum.Gender
import model.{GenderEnum, User}
import supertagged.{@@, lifterF}

object UserRepo {

  import DoobieSupport._

  def getById(id: Long): doobie.ConnectionIO[User] =
    sql"select id, firstname, lastname, age, gender from users where id = $id".query[User].unique

  def getAll: doobie.ConnectionIO[Seq[User]] =
    sql"select id, firstname, lastname, age, gender from users".query[User].to[Seq]
}
