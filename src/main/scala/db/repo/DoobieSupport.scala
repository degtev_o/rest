package db.repo

import doobie.util.Meta
import model.GenderEnum
import model.GenderEnum.Gender
import model.TaggedTypes.{Age, Firstname, Id, Lastname}

object DoobieSupport {

  implicit val idMeta: Meta[Id] = Meta[Long].imap(Id @@ _)(_.toLong)
  implicit val firstnameMeta: Meta[Firstname] = Meta[String].imap(Firstname @@ _)(_.toString)
  implicit val lastnamenameMeta: Meta[Lastname] = Meta[String].imap(Lastname @@ _)(_.toString)
  implicit val ageMeta: Meta[Age] = Meta[Int].imap(Age @@ _)(_.toInt)
  implicit val genderMeta: Meta[Gender] = Meta[String].imap(GenderEnum.withName)(_.toString)
}
