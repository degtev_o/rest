package config

import monix.eval.Task
import pureconfig.error.{ConfigReaderFailures, ConfigValueLocation}
import pureconfig.generic.auto._

case class DbConf(driver: String, connection: String, user: String, password: String, connectionPoolSize: Int)

case class HttpConf(host: String, port: Int)

case class Config(dbConf: DbConf, httpConf: HttpConf)

object Config {

  def apply(): Task[Config] =
    Task.fromEither[ConfigReaderFailures, Config](e => new ConfigError(formatError(e)))(pureconfig.loadConfig[Config])

  private def formatError(errors: ConfigReaderFailures) =
    errors.toList.map(e => formatLocation(e.location) + e.description).mkString("\n")

  private def formatLocation(location: Option[ConfigValueLocation]) =
    location.fold("")(l => s"${l.description}: ")
}