package config

class ConfigError(msg: String, cause: Throwable = null) extends Throwable(msg, cause)