package rest

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.ExceptionHandler
import akka.stream.{ActorMaterializer, Materializer}
import akka.http.scaladsl.server.Directives._
import config.HttpConf

import scala.util.control.NonFatal

class HttpService(config: HttpConf) {
  implicit val actorSystem: ActorSystem = ActorSystem("rest")
  implicit val materializer: Materializer = ActorMaterializer()

  implicit val exceptionHandler: ExceptionHandler = ExceptionHandler {
    case NonFatal(e) => complete(
      HttpResponse(StatusCodes.InternalServerError, entity = s"$e - ${e.getStackTrace.mkString(",")}")
    )
  }


}
