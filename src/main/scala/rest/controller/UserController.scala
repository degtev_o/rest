package rest.controller

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import service.MasterService

class UserController(service: MasterService) extends Controller with FailFastCirceSupport {

  override def route: Route = getAll

  private val uri = "user"

  import MonixSupport.taskToResponseMarshallable

  private val getAll = (get & path(uri)) {
    complete(service.user.getAll)
  }
}
