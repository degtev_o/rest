name := "rest"

version := "0.1"

scalaVersion := "2.12.8"

scalacOptions += "-Ypartial-unification"

val supertaggedV = "1.4"
val `akka-httpV` = "10.1.9"
val `akka-streamsV` = "2.5.23"
val pureconfigV = "0.11.1"
val monixV = "3.0.0-RC3"
val logbackV = "1.2.3"
val `scala-loggingV` = "3.9.2"
val doobieV = "0.7.0"
val circeV = "0.11.1"
val `akka-http-circeV` = "1.27.0"

libraryDependencies ++= Seq(
  "org.rudogma" %% "supertagged" % supertaggedV,
  "com.typesafe.akka" %% "akka-http" % `akka-httpV`,
  "com.typesafe.akka" %% "akka-stream" % `akka-streamsV`,
  "com.github.pureconfig" %% "pureconfig" % pureconfigV,
  "io.monix" %% "monix" % monixV,
  "ch.qos.logback" % "logback-classic" % logbackV,
  "com.typesafe.scala-logging" %% "scala-logging" % `scala-loggingV`,
  "org.tpolecat" %% "doobie-core" % doobieV,
  "org.tpolecat" %% "doobie-hikari" % doobieV,
  "org.tpolecat" %% "doobie-postgres" % doobieV,
  "io.circe" %% "circe-core" % circeV,
  "io.circe" %% "circe-generic" % circeV,
  "io.circe" %% "circe-parser" % circeV,
  "de.heikoseeberger" %% "akka-http-circe" % `akka-http-circeV`
)